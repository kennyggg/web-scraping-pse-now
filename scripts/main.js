const rp = require('request-promise');
const $ = require('cheerio');
const redis = require('redis');
const client = redis.createClient();
const cron = require('node-cron');
const url = 'https://www.pesobility.com/stock/NOW';
let stockPrice;  // 
let change;
let stocks;


function initRedis(quote){
  
  client.on('error', function(err){
    console.log('Something went wrong ', err)
  });

  var d = new Date();
    stocks = {
     price: stockPrice,
     percent: change, 
     time: d.getHours() +":"+ d.getMinutes(),
     month: d.getMonth()+" ",
     day: d.getDate()+" ",
     year: d.getFullYear()+" "
    }
    client.set('Now Corporation', JSON.stringify(stocks), redis.print);
    console.log("Successfully updated");

    client.get('Now Corporation', function(err, result) {
      console.log(result); // this is a string
      console.log(err);
      quote = result;
      return quote;
  });
   
}

function valueSlice(){

  var spaceCount=0;
  var base;
  var parenthesis;
  var percent;
  
  rp(url).then(function(html){
   base = $('#MAIN_CONTENT_COL > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div:nth-child(2) > span:nth-child(1)',html).text();  //css selector of the stock quote
      // lousy for loop implementation to find the space,parenthesis, and %  from this format[ 'P4.50 (-4.05%)\n'] as basis when splicing the stock values and percent change value.
   for(x=0;x<30;x++){
    for(y=0;y<30;y++){
     if(base.slice(x,y)==' '){
          spaceCount = y;
     }else if(base.slice(x,y)=='('){
          parenthesis = y;
     }else if(base.slice(x,y)=='%'){
          percent = y;
     }
    }
  }
          // extract the values from unnecessary letters
  stockPrice = base.slice(1,spaceCount);
  change = base.slice(parenthesis,percent-1);
  console.log("stock is "+stockPrice);
  console.log("Percent is "+change);
  console.log(spaceCount); 
  })


}
  // var validate = cron.validate('0,15,30 9,17 * * *');      use this to check if cron value is valid
  // console.log(validate);

  cron.schedule('0,15,30 9,17 * * *', () => {
    valueSlice();
    setTimeout(initRedis, 60000);
    console.log('Runing a job at 9:00am to 5:00pm Timezone of Asia Kuala lumpur');
  }, {
    scheduled: true,
    timezone: "Asia/Kuala_Lumpur"
  });
  










