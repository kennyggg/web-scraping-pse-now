# Web Scraping PSE Now



# Modules
--------------------------------------------------------------------------------------------------------------------------------
 	npm  install request-promise       // retrieval of promise
	npm  install cheerio                     // the web scraper 
	npm  install redis		     // persistent database using ram	
	npm  install node-cron                 // job scheduler 
--------------------------------------------------------------------------------------------------------------------------------


# Initial Block

-- comprises mostly of the  dependencies coded. 
--------------------------------------------------------------------------------------------------------------------------------
    const rp = require('request-promise'); 
    const $ = require('cheerio');
    const redis = require('redis');
    const client = redis.createClient();  //here we create the client redis 
    const cron = require('node-cron');   
    const url = 'https://www.pesobility.com/stock/NOW'; // Now stock url
    let stockPrice;  // the stock price
    let change;	     // Percentage change  of stock if up or down
    let stocks;     // the object to hold all of the value for json
--------------------------------------------------------------------------------------------------------------------------------





# Redis Code
--------------------------------------------------------------------------------------------------------------------------------
    https://github.com/MicrosoftArchive/redis/releases            and run the redis-server
    This is where most of the redis code function such as storing and retrieval
    The client.set() function is used to write and overwrite  the object(stocks) which is stringified first since the database only accepts strings.
    The client.get() returns the string of the retrieved value using key

 	
    function initRedis(quote){            
      client.on('error', function(err){
       console.log('Something went wrong ', err)
     });
    
     var d = new Date();			
       stocks = {	
       price: stockPrice,
       percent: change,
       time: d.getHours() +":"+ d.getMinutes(),
       month: d.getMonth()+" ",
       day: d.getDate()+" ",
       year: d.getFullYear()+" "
       }
       //
       client.set('Now Corporation', JSON.stringify(stocks), redis.print);
       console.log("Successfully updated");  
    
       client.get('Now Corporation', function(err, result) {
         console.log(result); // this is a string
         console.log(err);
         quote = result;
         return quote;
     });
     
    }

--------------------------------------------------------------------------------------------------------------------------------



# Web Scraping Module

    This is where the stock value is retrieved/scraped.
    The  rp(url).then(function(html) acts as a base using the thennable to retrieve the url from the “request-promise” module
    The base(variable) and  $ is the web scraper which uses the #MAIN_CONTENT…. As the CSS Selector(Path)  
    Since the scraped value result in concatenated string such as  P4.50 (-4.05%)  format which is due to the site. I had to resort in slicing the string using for loop the get the proper value.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     	
    function valueSlice(){
    
     var spaceCount=0;
     var base;
     var parenthesis;
     var percent;
      rp(url).then(function(html){
      base = $('#MAIN_CONTENT_COL > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div:nth-child(2) > span:nth-child(1)',html).text();  //css selector of the stock quote
         // lousy for loop implementation to find the space,parenthesis, and %  from this format[ 'P4.50 (-4.05%)\n'] as basis when splicing the stock values and percent change value.
      for(x=0;x<30;x++){
       for(y=0;y<30;y++){
        if(base.slice(x,y)==' '){
             spaceCount = y;
        }else if(base.slice(x,y)=='('){
             parenthesis = y;
        }else if(base.slice(x,y)=='%'){
             percent = y;
        }
       }
     }
          // extract the values from unnecessary letters
     stockPrice = base.slice(1,spaceCount);
     change = base.slice(parenthesis,percent-1);
     console.log("stock is "+stockPrice);
     console.log("Percent is "+change);
     console.log(spaceCount);
     })


}
--------------------------------------------------------------------------------------------------------------------------------




# Cron Job Module
--------------------------------------------------------------------------------------------------------------------------------

This is where the scheduler for 9am and  5pm with redundancies every 15 minutes in case the 0 minute mark fails.
https://www.npmjs.com/package/node-cron


       // var validate = cron.validate('0,15,30 9,17 * * *');      use  this   to check if cron value is valid
     // console.log(validate);
    
     cron.schedule('0,15,30 9,17 * * *', () => {
       valueSlice();
       setTimeout(initRedis, 60000);
       console.log('Runing a job at 9:00am to 5:00pm Timezone of Asia Kuala lumpur');
     }, {
       scheduled: true,
       timezone: "Asia/Kuala_Lumpur"
     });

--------------------------------------------------------------------------------------------------------------------------------


